#Building a professional blog from scratch.

This blog was entirely programmed manually, no library was included in it, and of course no dependancy manager used.
This blog is following the MVC architecture, so the data manipulation, the views and the controllers are doing their work independantly with no interference between each other.

##USE IT LOCALLY
Clone the project on your machine
Import the sql folder with you SGBD tool
Edit the dev.php to meet your local machine requirement (depending on you OS)
You should be able to access the blog locally


